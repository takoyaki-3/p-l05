$arr = []

def initialize_array
  $arr=[]
  return $arr
end

def get_array 
  return $arr
end

def append(x)
  $arr.push(x)
  return $arr
end

def swap(i, j)
  return $arr if j >= $arr.length
  t=$arr[i]
  $arr[i] = $arr[j]
  $arr[j]=t
  return $arr
end
