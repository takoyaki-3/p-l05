$bucket = [0,0]
$y = [4,7]

def initialize_bucket
  $bucket = [0,0]
  return $bucket
end

def empty(i)
  $bucket[i] = 0
  return $bucket
end

def fill(i)
  $bucket[i] = $y[i]
  return $bucket
end

def ano(i)
  return 0 if i == 1
  return 1 if i == 0
end

def transfer(i)
  n = $y[ano(i)] - $bucket[ano(i)]
  m = 0
  m = n if $bucket[i] > n
  m = $bucket[i] if $bucket[i] <= n

  $bucket[i] = $bucket[i] - m
  $bucket[ano(i)] = $bucket[ano(i)] + m

  return $bucket
end
