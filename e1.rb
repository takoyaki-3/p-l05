$balance = 0

def initialize_balance
  $balance=0
end

def get_balance
  return $balance
end

def deposit(x)
  $balance = $balance+x
  return $balance
end

def withdraw(x)
  $balance = $balance-x
  $balance = 0 if $balance < 0
  return $balance
end
