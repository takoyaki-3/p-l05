$board = []

def initialize_board(n)
  $board = Array.new(2*n+1,0)
  n.times do |i|
    $board[i] = 1
    $board[2*n - i] = 2
  end
  return $board
end

$k = [0,1,-1]
$k2 = [0,2,-2]

def move(i)
  n =$board[i] 
  if($board[i + $k[n]] == 0)
    $board[i + $k[n]] = n
    $board[i] = 0
  end
  if($board[i + $k2[n]] == 0 && $board[i + $k[n]]!=n)
    $board[i + $k2[n]] = n
    $board[i] = 0
  end
  return $board
end
